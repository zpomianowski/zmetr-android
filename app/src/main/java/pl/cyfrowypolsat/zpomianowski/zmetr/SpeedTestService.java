package pl.cyfrowypolsat.zpomianowski.zmetr;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Queue;
import java.util.Random;

public class SpeedTestService extends Service {

    public static final long INTERVAL = 1000;

    public ObservableSpeed mDLSpeed;
    public ObservableSpeed mULSpeed;
    public int maxDL = 10;
    public int maxUL = 10;

    private LocalBinder mBinder = new LocalBinder();
    private NotificationManager mNm;
    private SpeedMeter mSpeedMeter;

    final ArrayDeque<TPMeasurement> mHistory = new ArrayDeque<TPMeasurement>(MainActivity.SAMPLES);

    public class LocalBinder extends Binder {
        SpeedTestService getService() {
            return SpeedTestService.this;
        }
    }

    public Object[] getmHistory() {
        return mHistory.toArray();
    }

    private class SpeedMeter extends Thread {
        private final int N = 50;
        private boolean runFlag = true;
        private NetworkInfo mNet;

        public SpeedMeter() {
            mDLSpeed.setValue(0);
            mULSpeed.setValue(0);
        }

        public boolean isOnline() {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            mNet = cm.getActiveNetworkInfo();
            return (mNet != null && mNet.isConnected());
        }

        public void stopMeasuring() {
            runFlag = false;
        }

        @Override
        public void run() {
            int inc = 0;
            long oldTS = SystemClock.uptimeMillis();
            long oldDL = TrafficStats.getMobileRxBytes();
            long oldUL = TrafficStats.getMobileTxBytes();
//            long oldDL = TrafficStats.getTotalRxBytes();
//            long oldUL = TrafficStats.getTotalTxBytes();

            while (runFlag) {
                try {
                    if (!isOnline()) {
                        mDLSpeed.setValue(0);
                        mULSpeed.setValue(0);
                        Thread.sleep(INTERVAL);
                        continue;
                    }

                    Thread.sleep(INTERVAL);

                    long newDL = TrafficStats.getMobileRxBytes();
                    long newUL = TrafficStats.getMobileTxBytes();
//                    long newDL = TrafficStats.getTotalRxBytes();
//                    long newUL = TrafficStats.getTotalTxBytes();
                    long newTS = SystemClock.uptimeMillis();
                    float diffDL = newDL - oldDL;
                    float diffUL = newUL - oldUL;
                    long diffTS = newTS - oldTS;
                    // 1000 * 8 / 1024 / 1024 = 0.007629395
//                    float dlMb = (float) ((diffDL * 0.007629395)) / diffTS;
//                    float ulMb = (float) ((diffUL * 0.007629395)) / diffTS;
                    float shift = (float) new Random().nextGaussian();
                    float dlMb = 953 + shift * 10;
                    float ulMb = 29 + shift * 3;
                    maxDL = Math.max(maxDL, (int) (Math.ceil(dlMb/N) * N));
                    maxUL = Math.max(maxUL, (int) (Math.ceil(ulMb/N) * N));
//                    maxTP = Math.max(maxDL, maxUL);
                    mDLSpeed.setValue(dlMb);
                    mULSpeed.setValue(ulMb);
                    oldDL = newDL;
                    oldUL = newUL;
                    oldTS = newTS;
                    inc++;

                    mHistory.poll();
                    mHistory.add(new TPMeasurement(dlMb, ulMb));

                    Log.d("SpeedTestService",
                            String.format(
                                    "DL: %.2f, UL: %.2f, diff [ms]: %d",
                                    mDLSpeed.getValue(),
                                    mULSpeed.getValue(),
                                    diffTS));

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        Log.d("MAIN", "START!!!!");
        super.onCreate();
        for (int i = 0; i < MainActivity.SAMPLES; i++) {
            mHistory.add(new TPMeasurement());
        }
        mNm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mDLSpeed = new ObservableSpeed("DL");
        mULSpeed = new ObservableSpeed("UL");
        mSpeedMeter = new SpeedMeter();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!mSpeedMeter.isAlive()) mSpeedMeter.start();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        mSpeedMeter.stopMeasuring();
        mNm.cancel(R.string.app_name);
        super.onDestroy();
    }

    private void showNotification(CharSequence title, CharSequence text,
                                  CharSequence bigText, Boolean active) {

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);

        Drawable drawable = getApplicationInfo().loadIcon(getPackageManager());
        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

        NotificationCompat.Builder notify_msg;
        notify_msg = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setContentIntent(contentIntent)
                .setContentTitle(title)
//                .setSubText("sub text")
//                .setNumber(123)
                .setOngoing(true)
                .setTicker("a")
                .setSmallIcon(getApplicationInfo().icon);

//        if (active) notify_msg.setSubText(getResources().getText(R.string.online));
//        else notify_msg.setSubText(getResources().getText(R.string.offline));

        if (bigText != null) {
            notify_msg.setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(bigText));
        }

        mNm.notify(R.string.app_name, notify_msg.build());
    }
}
