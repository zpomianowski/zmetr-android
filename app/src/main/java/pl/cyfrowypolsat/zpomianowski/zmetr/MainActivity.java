package pl.cyfrowypolsat.zpomianowski.zmetr;

import android.os.Handler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.anastr.speedviewlib.SpeedView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {

    public static final int SAMPLES = 60;

    private BarChart mChart;
    private boolean mBound = false;

    private SpeedTestService mSTService;
    private ServiceConnection mSTServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mSTService = ((SpeedTestService.LocalBinder) service).getService();
            mSTService.mDLSpeed.addObserver(MainActivity.this);
            mSTService.mULSpeed.addObserver(MainActivity.this);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        mChart = (BarChart) findViewById(R.id.chart);
        Description desc = new Description();
        desc.setText("");
        mChart.setDescription(desc);

        // set axis
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new DateXAxisFormatter());
        xAxis.setLabelCount(MainActivity.SAMPLES / 10);

        YAxis yAxis = mChart.getAxisLeft();
        yAxis.setDrawAxisLine(false);
        yAxis.setAxisMinimum(0);
        yAxis.setLabelCount(11);
        mChart.getAxisRight().setEnabled(false);
        mChart.getAxisRight().setDrawAxisLine(false);

        setupData(SAMPLES);
        mChart.invalidate();
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent i = new Intent(this, SpeedTestService.class);
        bindService(i, mSTServiceConn, Context.BIND_AUTO_CREATE);
        startService(i);
    }

    public void onStop() {
        super.onStop();
        unbindService(mSTServiceConn);
    }

    @Override
    public void onDestroy() {
        Intent i = new Intent(this, SpeedTestService.class);
        stopService(i);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // do not add menu - we have no options so far :P
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupData(int count) {
        ArrayList<BarEntry> dlVals = new ArrayList<BarEntry>();
        ArrayList<BarEntry> ulVals = new ArrayList<BarEntry>();
        ArrayList<BarEntry> diffVals = new ArrayList<BarEntry>();

        float range = 20;
        for (int i = 0; i < count; i++) {
            ulVals.add(new BarEntry(i, 0));
            dlVals.add(new BarEntry(i, 0));
            diffVals.add(new BarEntry(i, 0));
        }

        BarDataSet set_dl;
        BarDataSet set_ul;
        BarDataSet set_diff;

        set_dl = new BarDataSet(dlVals, getString(R.string.tp_dl));
        set_ul = new BarDataSet(ulVals, getString(R.string.tp_up));
        set_diff = new BarDataSet(diffVals, getString(R.string.tp_mutual));

        confData(set_dl, ContextCompat.getColor(this, R.color.colorDL_mSpeed));
        confData(set_ul, ContextCompat.getColor(this, R.color.colorUL_mSpeed));
        confData(set_diff, ContextCompat.getColor(this, R.color.colorPrimary));

        List<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(set_dl);
        dataSets.add(set_ul);
        dataSets.add(set_diff);


        // create a data object with the datasets
        BarData data = new BarData(dataSets);
        data.setValueTextSize(9f);
        data.setDrawValues(false);

        // set data
        mChart.setData(data);
//        }
    }

    private void updateData() {
        if (mSTService == null) return;

        ArrayDeque data = mSTService.mHistory;

        ArrayList<BarEntry> dlVals = new ArrayList<BarEntry>();
        ArrayList<BarEntry> ulVals = new ArrayList<BarEntry>();
        ArrayList<BarEntry> diffVals = new ArrayList<BarEntry>();

        for (int i = 0; i < data.size(); i++) {
            TPMeasurement item = (TPMeasurement) data.toArray()[i];
            dlVals.add(new BarEntry(i, item.dl));
            ulVals.add(new BarEntry(i, item.ul));
            diffVals.add(new BarEntry(i, item.diff));
        }

        if (mChart.getData() != null &&
            mChart.getData().getDataSetCount() > 0) {
            BarDataSet set_dl = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set_dl.setValues(dlVals);
            BarDataSet set_ul = (BarDataSet) mChart.getData().getDataSetByIndex(1);
            set_ul.setValues(ulVals);
            BarDataSet set_diff = (BarDataSet) mChart.getData().getDataSetByIndex(2);
            set_diff.setValues(diffVals);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        }
    }

    private void confData(BarDataSet set_dl, int col) {
//        set_dl.setMode(BarDataSet.Mode.CUBIC_BEZIER);
//        set_dl.setCubicIntensity(0.2f);
//        set_dl.setDrawFilled(true);
//        set_dl.setDrawCircles(false);
//        set_dl.setLineWidth(1.8f);
//        set_dl.setCircleRadius(4f);
//        set_dl.setCircleColor(col);
//        set_dl.setHighLightColor(Color.rgb(244, 117, 117));
        set_dl.setColor(col);
//        set_dl.setFillColor(col);
//        set_dl.setFillAlpha(100);
//        set_dl.setDrawHorizontalHighlightIndicator(false);
//        set_dl.setFillFormatter(new IFillFormatter() {
//            @Override
//            public float getFillLinePosition(IBarDataSet dataSet, BarDataProvider dataProvider) {
//                return -10;
//            }
//        });
    }

    @Override
    public void update(Observable observable, Object o) {
        ObservableSpeed param = (ObservableSpeed) observable;
        int identifier = -1;
        int speedMax = 10;
        switch (param.getName()) {
            case "DL":
                identifier = R.id.speedDL;
                speedMax = mSTService.maxDL;
                break;
            case "UL":
                identifier = R.id.speedUL;
                speedMax = mSTService.maxUL;
                break;
        }
        final SpeedView s = (SpeedView) findViewById(identifier);
        final float data = (float) o;
        final int M = speedMax;

        updateData();

        s.post(new Runnable() {
            @Override
            public void run() {
                s.setMaxSpeed(M);
                s.speedTo(data, SpeedTestService.INTERVAL);
//                mChart.getAxisLeft().setAxisMaximum(totalM);
                mChart.invalidate();
            }
        });
//        Log.w("OBSERVER", String.format("%s: %f", param.getName(), o));
    }
}
