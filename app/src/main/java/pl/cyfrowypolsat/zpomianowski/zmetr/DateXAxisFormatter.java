package pl.cyfrowypolsat.zpomianowski.zmetr;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

/**
 * Created by zpomianowski on 29.09.17.
 */

public class DateXAxisFormatter implements IAxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
//        Long tsLong = System.currentTimeMillis()/1000;
        return String.format("+%.0fs", value);
    }
}
