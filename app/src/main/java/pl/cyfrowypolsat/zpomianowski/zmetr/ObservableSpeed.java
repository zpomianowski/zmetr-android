package pl.cyfrowypolsat.zpomianowski.zmetr;

import java.util.Observable;
import java.io.Serializable;

/**
 * Created by zpomianowski on 25.09.17.
 */

public class ObservableSpeed extends Observable implements Serializable {
    private float value;
    private String name;

    public ObservableSpeed(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
        this.setChanged();
        this.notifyObservers(value);
    }
}
