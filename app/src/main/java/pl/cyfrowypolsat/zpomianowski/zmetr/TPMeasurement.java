package pl.cyfrowypolsat.zpomianowski.zmetr;

/**
 * Created by zpomianowski on 29.09.17.
 */

public class TPMeasurement {
    public float dl = 0;
    public float ul = 0;
    public float diff = 0;

    public TPMeasurement(float dl, float ul) {
        this.dl = dl;
        this.ul = ul;
        this.diff = Math.min(dl, ul);
    }

    public TPMeasurement() {}
}
